const { createApp } = Vue;

createApp({
  data() {
    return {
      username: "",
      password: "",
      error: null,
      sucesso: null,
      userAdmin: false,

      //Declaracao dos arrays para armazenamento de usúarios e senhas
      usuarios: ["admin", "adriano", "julia"],
      senhas: ["1234", "1234", "1234"],
    };
  }, //Fechamento data

  methods: {
    login() {
      setTimeout(() => {
        // alert("Dentro do setTimeout!");
        if (
          (this.username === "Adriano" && this.password === "12342023") ||
          (this.username === "Julia" && this.password === "12342023") ||
          (this.username === "Admin" && this.password === "admin12345")
        ) {
          this.sucesso = "Login efetuado com sucesso!";
          this.error = null;
          if (this.username === "Admin") {
            this.userAdmin = true;
          }
        } else {
          //alert("Login não efetuado!");
          this.error = "Nome ou senha incorretos!";
          this.sucesso = null;
        }
      }, 1000);
      //alert("Saiu do setTimeout!!!");
    }, //Fechamento login

    login2() {
      const index = this.usuarios.indexOf(this.username);
      if (index !== -1 && this.senhas[index] === this.password) {
        this.error = null;
        this.sucesso = "Login 2 efetuado com sucesso!";
        if (this.username === "admin" && index === 0){
          this.userAdmin = true;
        }
      } //Fechamento if
      else{
        this.error = "Nome ou senha incorretos!";
        this.sucesso = null;
      }
    }, //Fechamento Login2

    paginaCadastro() {
      if (this.userAdmin == true) {
        this.sucesso = "Login ADM identificado!";

        localStorage.setItem("userAdmin", this.userAdmin);

        setTimeout(() => {
          window.location.href = "paginaCadastro.html";
        }, 1000); //Fechamento setTimeout
      } else {
        this.error = "Faça login com usúario administrativo!";
      }
    }, //Fechamento paginaCadastro

    adicionarUsuario(){
        this.userAdmin =localStorage.getItem("userAdmin");

        if(this.userAdmin === true){
            this.sucesso = "Logado como ADM!!";
            this.error = null;
        }//Fechamento if
        else{
            this.error ="Usúario NÂO ADM!!";
            this.sucesso = null;
        }
    }, //Fechamento adicionarUsuario
  }, //Fechamento methods
}).mount("#app");
