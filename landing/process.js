const {createApp} = Vue;

createApp({
    data(){
        return{
            products:[
                {
                    id: 1,
                    name: "Tenis",
                    description: "Um par de tenis confortavel para esportes",
                    price: 1299.99,
                    image: "./imagens/imgTenis.jpg"
                },//Fechamento Item 1
                {
                    id:2,
                    name:"Botas",
                    description: "Botas elegantes para qualquer ocasião",
                    price: 899.99,
                    image: "./imagens/imgBotas.jpg"
                },//Fechamento Item 2
                {
                    id: 3,
                    name: "Sapatos",
                    description: "Sapatos clássicos para um visual sofisticado",
                    price: 249.99,
                    image: "./imagens/imgSapatos.jpg",
                },//Fechamento item 3
                {
                    id: 4,
                    name: "Sandálias",
                    description: "Sandalias confortaveis para os seus pés",
                    price: 589.99,
                    image: "./imagens/imgSandalias.jpg",
                },//Fechamento item 4
            ],//Fechamento products
            currentProduct: {}, //produto atual
            cart: [],

        };//Fechamento return
    },//Fechamento data

    mounted(){
        window.addEventListener("hashchange", this.updateProduct);

    },//Fechamento mounted

    //Função VUE para retornar resultados espeficifos de um bloco programado
    computed:{
        cartItemCount(){
            return this.cart.length;
        },//Fechamento cartItemCount
            
        cartTotal(){
            return this.cart.reduce((total, product) => total + product.price,0);
        },//Fechamento cartTotal
     },//Fechamento computed

    methods:{
        updateProduct(){
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};

        },//Fechamento updateProduct

        addToCart(product){
            this.cart.push(product);
        },//Fechamento addTocart

        removeFromCart(product){
            const index = this.cart.indexOf(product);
            if(index != -1){
                this.cart.splice(index,1);
            }
        },//Fechamento remove
    },//Fechamento methods
}).mount("#app"); //Fechamento createApp